<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;

class ActivityAPIController extends Controller
{
    public function createActivity(Request $request)
    {
        $activity = new Activity();
        $activity->activity_name = $request->name;
        $activity->save();
        echo "Successful";
    }

    public function getActivity()
    {
        return Activity::all();
    }

    public function updateActivity($id)
    {
        $activity = Activity::find($id);
        $activity->activity_name = "Jumping";
        $activity->save();
        return $activity;
    }
}
